# Compliance Configurations

### **Note: Do not edit this project directly, but rather clone it and configure it within your own GitLab instance.**

This project houses the complianmce pipeline which can be applied using [compliance frameworks](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html). These configurations are applied to the [Accounting Department](https://gitlab.com/gitlab-de/tutorials/security-and-governance/compliance-frameworks/accounting-department) application allowing for separately managed jobs to run on this project. Compliance pipelines enable [separation of duties](https://csrc.nist.gov/glossary/term/separation_of_duty) by allowing a member of a compliance team to generate a separate pipelines with separate permissions. This allows compliance officers to maintain a separate pipeline which cannot be disabled such as running security scans, license compliance, etc.

Examples of these can be see in the [Accounting Department](https://gitlab.com/gitlab-de/tutorials/security-and-governance/compliance-frameworks/accounting-department) application, which shows the separate pipelines being run from this repo along with the actual pipelines that are part of the project.

**Note**: These features are only available in GitLab Ultimate

## Project Contents

This project contains the following CICD template:

- **.soc2-compliance.yml**: Makes sure that certain jobs are run to maintain SOC2 compliance

**Note:** This template doesn't actually make the application [SOC2](https://en.wikipedia.org/wiki/System_and_Organization_Controls) compliant, it is just examples used to showcase how separation of duties can be applied to achieve/maintain compliance. The CICD pipelines must be written yourself to achieve compliance, the above are just examples.

## How it works (Tutorial)

This goes over how to implement and review compliance pipelines using the projects in this sub-group

### Creating and Importing Projects

1. Create a new [sub-group](https://docs.gitlab.com/ee/user/group/subgroups/index.html) to house this tutorial

2. [Import Repo by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) for the [Accounting Department](https://gitlab.com/gitlab-de/tutorials/security-and-governance/compliance-frameworks/accounting-department) application into your sub-group

3. [Import Repo by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) for [this project](https://gitlab.com/gitlab-de/tutorials/security-and-governance/compliance-frameworks/compliance-configurations) into your sub-group

### Editting Compliance Pipeline Files

1. Edit the **.soc2-compliance.yml** file so that the `CI_PROJECT_PATH` points to your project, meaning edit **gitlab-de/tutorials/security-and-governance/compliance-frameworks/compliance-configurations** to your subgroup/project containing the compliance files

```yaml
include:
  - project: '$CI_PROJECT_PATH'
    file: '$CI_CONFIG_PATH'
    ref: '$CI_COMMIT_SHA'
    rules:
      - if: $CI_PROJECT_PATH != "gitlab-de/tutorials/security-and-governance/compliance-frameworks/compliance-configurations"
```

### Creating a compliance framework

1. Go to your top-level [group](https://docs.gitlab.com/ee/user/group/)

2. Go to **Settings > General** in the side-bar

3. Expand the **Compliance frameworks** section

4. Click on **Add a framework**

5. Add the following items:

- **Name**: Name of the framework
- **Description**: Description of the framework
- **Compliance Pipeline Configuration**: Path to a compliance file to run
- **Background Color**: Color of the framework

6. Click **Add framework**

### Run pipeline without the compliance framework

1. Go to the Accounting Department project you created

2. [Run the pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually)

3. Notice that only the jobs that run are as follows:

- get_total_yearly_income
- get_total_monthly_income
- generate_bar_chart

### Apply a compliance framework

1. Go to the Accounting Department project you created

2. Go to **Settings > General** in the side-bar

3. Expand the **Compliance framework** section

4. Select the compliance framework you created

5. Press **Save Changes**

### Run pipeline with the complaince framework

1. Go to the Accounting Department project you created

2. [Run the pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually)

3. Notice that additional jobs defined in the compliance framework are also run

- get_total_yearly_income
- get_total_monthly_income
- generate_bar_chart
- secret_detection
- semgrep_sast
- soc2_compliance_check

4. Success! 🎉

---

Created and maintained by [Fern](https://gitlab.com/fjdiaz)🌿

- [LinkedIn](https://www.linkedin.com/in/awkwardferny/)
- [Twitter](https://twitter.com/awkwardferny)